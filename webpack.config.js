/* eslint-disable */

const path = require('path');

module.exports = {
	entry: './src/index.js',
	output: {
		path: path.resolve(__dirname, 'extension-dist'),
		filename: './bundle.js'
	},
	module: {
		rules: [
			{ test: /\.js$/, exclude: /node_modules|extension-dist/, loader: "eslint-loader", enforce: "pre", options: { emitError: true, failOnWarning: true, failOnError: true } },
			{ test: /\.js$/, exclude: /node_modules|extension-dist/, loader: "babel-loader" }
		]
	}
};