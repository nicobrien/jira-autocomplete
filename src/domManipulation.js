import * as Constants from './constants';
import { getTemplateForIssue } from './templates';

/**
 * Flattens the issueTypeData from the Jira DOM, does not alter input
 * 
 * @param {Array} data An array of the form:
 * [{ items: [a, b, c, ...], ...}, { items: [x, y, z, ...], ...}, ...]
 * @returns {Array} A new array of the form
 * [a, b, c, ..., x, y, z, ...]
 * @private
 */
const flattenIssueData = (data) => {
	return data.reduce((accumulator, current) => {
			return accumulator.concat(current.items);
		}, []);
};

/**
 * Get all selected issue types from issue data.
 * 
 * There may be more than one issue in issueTypeData that has an attribute 
 * selected = true. To date, when this has occurred it has been observed that
 * the issues types are duplicates so it should be safe to select any, 
 * arbitrarly the first.
 * 
 * @param {Array} issueTypeData the Array of issue type objects which are 
 * stored in the Jira DOM. Expected to be of the form
 * @example
 *   [ 
 *     { 
 *       items: [
 *         { label: "New Feature", value: "1", selected: true, ... },
 *         { label: "Bug", value: "2", selected: false, ... },
 *         ...
 *       ], ...
 *     },...
 *   ]
 * @returns {Array} An array of selected issues type objects
 */
export const getSelectedIssues = (issueTypeData) => {
	return flattenIssueData(issueTypeData)
		.filter((item) => {
			return item.selected;
		});
};

/** 
 * Fill the description text area in the 'create issue' dialog with the 
 * template matching the issue param.
 * 
 * @param {HTMLElement} dialog The 'create issue' DOM element
 * @param {String} issue The name of the selected issue type
*/
export const populateDescriptionForIssue = (dialog, issue) => {
	let descriptionTextArea = dialog.querySelector(Constants.SELECTOR_FOR_DESCRIPTION_TEXTAREA);
	let template = getTemplateForIssue(issue);
	descriptionTextArea.value = template;
};