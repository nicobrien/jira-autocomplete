export const INTERVAL_MS = 500;

export const ISSUE_DIALOG_ID = 'create-issue-dialog';

// selectors are relative to the create issue dialog element
export const SELECTOR_FOR_ISSUE_OPTIONS = 'form[name=jiraform] div[data-field-id=issuetype] div#issuetype-options';

export const ISSUE_OPTION_DATA_ATTRIBUTE_NAME = 'data-suggestions';

export const SELECTOR_FOR_DESCRIPTION_TEXTAREA = 'div#description-wiki-edit textarea';

