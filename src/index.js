// entry point: should be bundled to extension-dist/bundle.js


import * as Constants from './constants';
import { 
	getSelectedIssues,
	populateDescriptionForIssue
} from './domManipulation';


/** 
 * Creates a function 'hasIssueSelectionChanged', which can keep track of the 
 * selected issue type so that we can determine if the selected issue has 
 * changed.
 * 
 * @param {String} issue The name of the selected issue to test
 * @returns {Boolean} True if the selection has changed, otherwise false
*/
const hasIssueSelectionChanged = (() => {
	let currentIssueSelection = null;
	return issue => {
		let hasChanged = issue !== currentIssueSelection;
		currentIssueSelection = issue;
		return hasChanged;
	};
})();


let hasError = false;

/** 
 * Main worker:
 * 
 * Looks for the existence of the 'create issue' dialog in jira page.
 * 
 * If such a dialog is found then we check if the issue type has changed and 
 * if it has, we update the description text area with the template that 
 * matches the selected issue type
*/
const pollFunc = () => {
	try {
		let issueDialog = window.document.getElementById(Constants.ISSUE_DIALOG_ID);
		if (issueDialog) {
			let issueTypeDataAttribute = issueDialog
				.querySelector(Constants.SELECTOR_FOR_ISSUE_OPTIONS)
				.getAttribute(Constants.ISSUE_OPTION_DATA_ATTRIBUTE_NAME);
			let issueTypeData = JSON.parse(issueTypeDataAttribute);
			let currentSelectedIssueTypes = getSelectedIssues(issueTypeData);
			if (hasError || currentSelectedIssueTypes.length > 0) {
				let issueName = currentSelectedIssueTypes[0].label;
				if (hasIssueSelectionChanged(issueName)) {
					console.debug(`Jira auto populate: current Issue ${issueName} selected`);
					populateDescriptionForIssue(issueDialog, issueName);
				}
				hasError = false;
			}
		}
	} catch (e) {
		hasError = true;
		console.warn('Jira auto populate: ', e);
	}
};


window.setInterval(pollFunc, Constants.INTERVAL_MS);
