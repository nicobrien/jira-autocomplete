import releaseRequestTemplate from './releaseRequestTemplate';
import improvementTemplate from './improvementTemplate';
import newFeatureTemplate from './newFeatureTemplate';
import defaultTemplate from './defaultTemplate';
import storyTemplate from './storyTemplate';
import epicTemplate from './epicTemplate';
import taskTemplate from './taskTemplate';
import bugTemplate from './bugTemplate';

const emptyTemplate = ``;

const descriptionTemplateLookup = {
	"Default": defaultTemplate,
	"Bug": bugTemplate,
	"Epic": epicTemplate,
	"Improvement": improvementTemplate,
	"New Feature": newFeatureTemplate,
	"Release Request": releaseRequestTemplate,
	"Story": storyTemplate,
	"Task": taskTemplate
};

/**
 * Get a template for a given issue type
 * 
 * @param {String} issue The name of the issue type for which to retrieve a 
 * description template
 * @example
 * var template = getTemplateForIssue('Bug');
 * @returns {String} The description template for the given issue type
 */
export const getTemplateForIssue = (issue) => {
	if (descriptionTemplateLookup.hasOwnProperty(issue)) {
		return descriptionTemplateLookup[issue];
	} else {
		console.warn(`Jira auto populate: could not find issue template for ${issue}`);
		return emptyTemplate;
	}
};
