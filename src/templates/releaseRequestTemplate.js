const releaseRequestTemplate = `
*+Please deploy the following:+*
Service: 
Version: 
Location: 
Cluster - LA and TX
Job: maintenance

*+Instructions for release:+*
- Please deploy the new build using the below configuration to the staging instance of production cluster

{code:xml}
{
    "service_name": "",
    "version": "",
    "release_tag": "",
    "enabled": true,
    "containers": [
        {
            "registry": "",
            "image": "",
            "tag": "",
            "endPoints": [
                {
                    "name": "",
                    "port": 80,
                    "protocol": "TCP"
                }
            ],
            "environment_variables": [
                {
                }
            ]
        }
    ],
    "constraints": {
    }
}
{code}


Rollback Procedure:
- 

Release signed off by: 
Pre Release Testing Complete and Passed: Yes / No
Post Release Testing Results: (to be filled in post release)
`;

export default releaseRequestTemplate;
