'''
    Generate default templates for those that we know about and print the
    imports required to use them
'''

todo = [
    'releaseRequestTemplate',
    'improvementTemplate',
    'newFeatureTemplate',
    'storyTemplate',
    'epicTemplate',
    'taskTemplate'
]


def getTemplate(name):
    return '''const {0} = `
*+Acceptance Criteria:+*
* 

*+Dev Notes:+*

*+Tester Notes:+*
`;

export default {1};
'''.format(name, name)


for name in todo:
    with open(name + '.js', mode='w') as f:
        f.writelines(getTemplate(name))
        f.close()
        print('import {0} from \'./{0}\'; '.format(name))
