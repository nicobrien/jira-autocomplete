# Browser extension for Jira

Will automatically fill in an issue template based on ticket type when creating a new Jira ticket.

Tempates are stored in `src/templates`.
Entry point is `src/index.js`.
Bundling with webpack.

